import {Routes} from '@angular/router';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { EmpresasComponent } from './empresas/empresas.component';
import { EmpresaComponent } from './empresas/empresa/empresa.component';

export const ROUTES: Routes = [
    {
    path:'',
    component: LoginComponent
    },
    {
    path:'login',
    component: LoginComponent
    },
    {
      path:'empresas',
      component: EmpresasComponent
    },
    {
      path:'empresa/:id', 
      component: EmpresaComponent
    }

]
