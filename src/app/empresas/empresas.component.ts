import { Component, OnInit } from '@angular/core';

import {EmpresasService} from './empresas.service';
import {Enterprises} from './empresa/empresa.model'
import {Enterprise} from './empresa/empresa.model'
import {EnterpriseUn} from './empresa/empresa.model'
import {DadosUn} from './empresa/empresa.model'
import {EnterpriseType} from './empresa/empresa.model'

@Component({
  selector: 'app-empresas',
  templateUrl: './empresas.component.html',
  styleUrls: ['./empresas.component.css']
})
export class EmpresasComponent implements OnInit {

  objeto: Enterprises;
  empresas:Enterprise[] = [];
  empresa: EnterpriseUn;

  vazio: boolean = true;

  constructor(private empresasService: EmpresasService){}

  listaTodasEmpresas(){
    this.empresasService.getEmpresas().subscribe(data => {this.objeto = data; this.empresas = this.objeto.enterprises; this.vazio = false;});
  }

  buscaEmpresaPorId(id:string){
    this.empresasService.getEmpresasPorId(id).subscribe(data => {this.empresa = data; console.log(this.empresa.enterprise.id)});
  }

  filtrarEmpresasTipoNome(parametro: string){
    this.empresasService.buscarEmpresas(parametro).subscribe(data => {this.objeto = data; this.empresas = this.objeto.enterprises; this.vazio = false;});
  }

  ngOnInit() {

  }
}
