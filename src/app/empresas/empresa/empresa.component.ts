import { Component, OnInit, Input} from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Enterprises } from './empresa.model'
import { EmpresasService } from './../empresas.service'
import {EnterpriseUn} from './empresa.model'
import {DadosUn} from './empresa.model'
import {EnterpriseType} from './empresa.model'
import {Enterprise} from './empresa.model'


@Component({
  selector: 'app-empresa',
  templateUrl: './empresa.component.html',
  styleUrls: ['./empresa.component.css']
})
export class EmpresaComponent implements OnInit {

  @Input() empresa:EnterpriseUn;

  foto: string;
  nome: string;
  descricao: string;

  constructor(private empresasService: EmpresasService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.empresasService.getEmpresasPorId(this.route.snapshot.params['id']).subscribe(data => {this.empresa = data;
      this.foto = this.empresa.enterprise.photo;
      this.nome = this.empresa.enterprise.enterprise_name;
      this.descricao = this.empresa.enterprise.description;
    })
  }
}
