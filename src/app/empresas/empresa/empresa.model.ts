export interface Enterprises {
  enterprises: Enterprise[];
}

export interface Enterprise {
  id: number;
  email_enterprise: any;
  facebook: any;
  twitter: any;
  linkedin: any;
  phone: any;
  own_enterprise: boolean;
  enterprise_name: string;
  photo: any;
  description: string;
  city: string;
  country: string;
  value: number;
  share_price: number;
  enterprise_type: EnterpriseType;
}

export interface EnterpriseType {
  id: number;
  enterprise_type_name: string;
}

export interface EnterpriseUn {
  enterprise: DadosUn;
  success: boolean;
}

export interface DadosUn {
  id: number;
  enterprise_name: string;
  description: string;
  email_enterprise?: any;
  facebook?: any;
  twitter?: any;
  linkedin?: any;
  phone?: any;
  own_enterprise: boolean;
  photo?: any;
  value: number;
  shares: number;
  share_price: number;
  own_shares: number;
  city: string;
  country: string;
  enterprise_type: EnterpriseType;
}
