import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { LoginService } from './login.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})


export class LoginComponent implements OnInit {

  constructor(private _loginService: LoginService){};

  logar(email, senha){
    var _login = this._loginService.autenticar(email,senha);
  }
  
  ngOnInit() {
  }
}
