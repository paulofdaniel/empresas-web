import { Injectable, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import {Headers} from '@angular/http'
import { CookieService } from 'ngx-cookie-service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';


@Injectable()
export class LoginService {
  constructor(private _httpClient : HttpClient, private _cookieService: CookieService, private router: Router) { }

  private _SERVIDOR_LOGIN = "http://54.94.179.135:8090/api/v1/users/auth/sign_in";

  private _objHeader;
  private _status;
  

  autenticar(email, senha) {

    this._httpClient.post(this._SERVIDOR_LOGIN,
      {
        email : email,
        password : senha
      }, {observe: 'response'})
      .subscribe(
        resposta => {
          this._objHeader = resposta.headers;
          this.gravarToken(this._objHeader);
          this.router.navigate(['/empresas']);
        }, erro => {
          this._cookieService.set( "access-token","");
        }
      )
    }

    gravarToken(obj:any){
      this._cookieService.set( "access-token", this._objHeader.get("access-token"));
      this._cookieService.set( "client", this._objHeader.get("client"));
      this._cookieService.set( "uid", this._objHeader.get("uid"));
    }


  }
