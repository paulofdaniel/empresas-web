import { Component } from '@angular/core';
import { EmpresasComponent } from './empresas/empresas.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [EmpresasComponent]
})
export class AppComponent{
  title = 'app';
}
