import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import { CommonModule } from '@angular/common';

import { ROUTES } from './app.routes'

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { EmpresasComponent } from './empresas/empresas.component';
import { EmpresasService } from './empresas/empresas.service';
import { LoginService } from './login/login.service';
import { EmpresaComponent } from './empresas/empresa/empresa.component';
import { Enterprises } from './empresas/empresa/empresa.model'
import { Enterprise } from './empresas/empresa/empresa.model'
import { EnterpriseType } from './empresas/empresa/empresa.model'
import { EnterpriseUn } from './empresas/empresa/empresa.model'
import { DadosUn } from './empresas/empresa/empresa.model'



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    EmpresasComponent,
    EmpresaComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    FormsModule,
    CommonModule,
    RouterModule.forRoot(ROUTES)
  ],
  providers: [LoginService, CookieService, EmpresasService],
  bootstrap: [AppComponent]
})
export class AppModule { }
